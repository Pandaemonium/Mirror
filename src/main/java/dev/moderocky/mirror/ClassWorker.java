package dev.moderocky.mirror;

import org.jetbrains.annotations.Nullable;

@SuppressWarnings("unchecked")
public interface ClassWorker<T> extends IMirror<T> {

    default Class<?> getAsClass() {
        T object = getTarget();
        return (object instanceof Class ? (Class<?>) object : object.getClass());
    }

    default <Q> Class<Q> getSuperclass() {
        T object = getTarget();
        return (object instanceof Class ? (Class<Q>) ((Class<?>) object).getSuperclass() : (Class<Q>) object.getClass().getSuperclass());
    }

    default <Q> Class<Q> getEnclosingClass() {
        T object = getTarget();
        return (object instanceof Class ? (Class<Q>) ((Class<?>) object).getEnclosingClass() : (Class<Q>) object.getClass().getEnclosingClass());
    }

    default <Q> Class<Q> getDeclaringClass() {
        T object = getTarget();
        return (object instanceof Class ? (Class<Q>) ((Class<?>) object).getDeclaringClass() : (Class<Q>) object.getClass().getDeclaringClass());
    }

    default <Q> @Nullable Q getEnclosingInstance() {
        T object = getTarget();
        Mirror<T> mirror = new Mirror<>(object);
        if (!mirror.hasField("this$0")) return null;
        return mirror.<Q>field("this$0").get();
    }

    default boolean isLocal() {
        return getAsClass().isLocalClass();
    }

}
