package dev.moderocky.mirror;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.function.Consumer;

@SuppressWarnings("unchecked")
public final class ConstructorMirror<T> implements IMirror<T> {

    private final transient @NotNull Constructor<T> constructor;
    private final transient @NotNull IMirror<?> parent;
    private final transient @NotNull Object target;

    public ConstructorMirror(@NotNull Constructor<T> constructor, @NotNull IMirror<?> parent, @NotNull Object target) {
        this.constructor = constructor;
        this.parent = parent;
        this.target = target;
        if (!constructor.isAccessible()) constructor.setAccessible(true);
        ReflectionCache.CACHE.cache(constructor, this);
    }

    public <Q extends Annotation> boolean hasAnnotation(Class<Q> annotation) {
        return constructor.getDeclaredAnnotation(annotation) != null;
    }

    public <Q extends Annotation> Q getAnnotation(Class<Q> annotation) {
        return constructor.getDeclaredAnnotation(annotation);
    }

    @Override
    public int getModifiers() {
        return constructor.getModifiers();
    }

    public int getParameterCount() {
        return constructor.getParameterCount();
    }

    public Class<?>[] getParameterTypes() {
        return constructor.getParameterTypes();
    }

    public Parameter[] getParameters() {
        return constructor.getParameters();
    }

    public boolean hasParameters() {
        return getParameterCount() == 0;
    }

    @Override
    public boolean isStatic() {
        return Modifier.isStatic(constructor.getModifiers());
    }

    @Override
    public boolean isFinal() {
        return Modifier.isFinal(constructor.getModifiers());
    }

    @Override
    public boolean isPublic() {
        return Modifier.isPublic(constructor.getModifiers());
    }

    @Override
    public String getName() {
        return constructor.getName();
    }

    public T newInstance(Object... params) {
        try {
            return constructor.newInstance(params);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public void newInstance(@Nullable Consumer<T> success, @Nullable Consumer<Throwable> failure, Object... params) {
        try {
            if (success != null) success.accept(newInstance(params));
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
        }
    }

    public boolean willAccept(Object... params) {
        Class<?>[] types = constructor.getParameterTypes();
        if (types.length == 0 && (params == null || params.length == 0)) return true;
        if (types.length != params.length) return false;
        for (int i = 0; i < types.length; i++) {
            if (!(params[i].getClass().equals(types[i]) || params[i].getClass().isAssignableFrom(types[i])))
                return false;
        }
        return true;
    }

    @Override
    public <Q> IMirror<Q> getParent() {
        return (IMirror<Q>) parent;
    }

    @Override
    public <Q> Q getTarget() {
        return (Q) target;
    }

    @Override
    public Constructor<T> getLiteral() {
        return constructor;
    }

}
