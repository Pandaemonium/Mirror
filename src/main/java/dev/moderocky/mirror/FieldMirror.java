package dev.moderocky.mirror;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Map;
import java.util.function.Consumer;

@SuppressWarnings("unchecked")
public final class FieldMirror<T> implements IMirror<T> {

    private final transient Field field;
    private final transient @NotNull IMirror<?> parent;
    private final transient Object target;

    public FieldMirror(@NotNull Field field, @NotNull IMirror<?> parent, @NotNull Object target) {
        this.field = field;
        this.parent = parent;
        this.target = target;
        if (!field.isAccessible()) field.setAccessible(true);
        if (isFinal() && isStatic()) try {
            new Mirror<>(field).field("modifiers", Field.class).set(field, field.getModifiers() & ~Modifier.FINAL);
        } catch (Throwable ignore) {
        }
        ReflectionCache.CACHE.cache(field, this);
    }

    public <Q extends Annotation> boolean hasAnnotation(Class<Q> annotation) {
        return field.getDeclaredAnnotation(annotation) != null;
    }

    public <Q extends Annotation> Q getAnnotation(Class<Q> annotation) {
        return field.getDeclaredAnnotation(annotation);
    }

    public T get() {
        if (target == null) return null;
        try {
            return (T) field.get(target);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public void get(Consumer<T> success, @Nullable Consumer<Throwable> failure) {
        try {
            if (success != null) success.accept(get());
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
        }
    }

    public void get(Consumer<T> success) {
        get(success, null);
    }

    public void set(T obj) {
        if (target == null) return;
        try {
            deconst().set(target, obj);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public T get(Object target) {
        try {
            return (T) field.get(target);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public void get(Consumer<T> success, @Nullable Consumer<Throwable> failure, Object target) {
        try {
            if (success != null) success.accept(get(target));
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
        }
    }

    public void get(Consumer<T> success, Object target) {
        get(success, null, target);
    }

    public void set(Object target, T obj) {
        try {
            deconst().set(target, obj);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public boolean isAnonymous() {
        return field.getType().isAnonymousClass();
    }

    public boolean isAnnotation() {
        return field.getType().isAnnotation();
    }

    public boolean isPrimitive() {
        return field.getType().isPrimitive();
    }

    public boolean isMap() {
        return field.getType() == Map.class || Map.class.isAssignableFrom(field.getType()) || field.getType().isInstance(Map.class);
    }

    public boolean isPlural() {
        return isArray() || isCollection();
    }

    public boolean isCollection() {
        return field.getType() == Collection.class || Collection.class.isAssignableFrom(field.getType()) || field.getType().isInstance(Collection.class);
    }

    @Override
    public String getName() {
        return field.getName();
    }

    public boolean isArray() {
        return field.getType().isArray();
    }

    public boolean isEnum() {
        return field.getType().isEnum();
    }

    public boolean isStatic() {
        return Modifier.isStatic(field.getModifiers());
    }

    public boolean isFinal() {
        return Modifier.isFinal(field.getModifiers());
    }

    public boolean isPublic() {
        return Modifier.isPublic(field.getModifiers());
    }

    public boolean isVolatile() {
        return Modifier.isVolatile(field.getModifiers());
    }

    public boolean isConstant() {
        return isStatic() && isFinal() && isPublic();
    }

    @Override
    public int getModifiers() {
        return field.getModifiers();
    }

    public Class<T> getType() {
        return (Class<T>) field.getType();
    }

    public Class<?> getDeclarator() {
        return field.getDeclaringClass();
    }

    @Override
    public <Q> IMirror<Q> getParent() {
        return (IMirror<Q>) parent;
    }

    @Override
    public <Q> Q getTarget() {
        return (Q) target;
    }

    private Field deconst() {
        try {
            if (!isConstant()) return field;
            new Mirror<>(field).field("modifiers").set(field.getModifiers() & ~Modifier.FINAL);
            return field;
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public boolean matches(FieldMirror<?> mirror) {
        if (!mirror.getName().equalsIgnoreCase(getName())) return false;
        if (mirror.isStatic() ^ this.isStatic()) return false;
        if (mirror.isEnum() ^ this.isEnum()) return false;
        return (mirror.getType().isAssignableFrom(this.getType()) || this.getType().isAssignableFrom(mirror.getType()) || mirror.getType().isInstance(this.getType()) || this.getType().isInstance(mirror.getType()));
    }

    public FieldMirror<T> getMatching(Mirror<?> mirror) {
        for (FieldMirror<T> fieldMirror : mirror.<T>getFieldMirrors()) {
            if (matches(fieldMirror)) return fieldMirror;
        }
        return null;
    }

    public boolean hasMatching(Mirror<?> mirror) {
        for (FieldMirror<?> fieldMirror : mirror.getFieldMirrors()) {
            if (matches(fieldMirror)) return true;
        }
        return false;
    }

    public void ifHasMatching(Mirror<?> mirror, Consumer<FieldMirror<T>> consumer) {
        tryCatch(() -> {
            FieldMirror<T> field = getMatching(mirror);
            if (field != null && consumer != null) consumer.accept(field);
        }, null);
    }

    @Override
    public Field getLiteral() {
        return field;
    }
}
