package dev.moderocky.mirror;

import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.util.function.Consumer;
import java.util.function.Function;

public interface IMirror<T> extends Modifiable, Reflective {

    <Q> IMirror<Q> getParent();

    <Q> Q getTarget();

    Object getLiteral();

    default Object getNativeLiteral() {
        return getLiteral();
    }

    <Q extends Annotation> boolean hasAnnotation(Class<Q> annotation);

    <Q extends Annotation> Q getAnnotation(Class<Q> annotation);

    default boolean removeModifier(int modifier) {
        return modifyAccess(getModifiers() & ~modifier);
    }

    default boolean addModifier(int modifier) {
        return modifyAccess(getModifiers() & modifier);
    }

    default boolean modifyAccess(int modifiers) {
        return tryCatch(() -> new Mirror<>(getNativeLiteral()).field("modifiers").set(modifiers));
    }

    int getModifiers();

    boolean isStatic();

    boolean isFinal();

    boolean isPublic();

    default boolean isPrivate() {
        return Modifier.isPrivate(getModifiers());
    }

    default boolean isAbstract() {
        return Modifier.isAbstract(getModifiers());
    }

    String getName();

    default <Q extends Annotation> void ifHasAnnotation(Class<Q> annotation, Consumer<Q> success) {
        if (hasAnnotation(annotation)) success.accept(getAnnotation(annotation));
    }

}
