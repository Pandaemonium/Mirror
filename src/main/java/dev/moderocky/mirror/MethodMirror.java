package dev.moderocky.mirror;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.function.Consumer;

@SuppressWarnings("unchecked")
public final class MethodMirror<T> implements IMirror<T> {

    private final transient @NotNull Method method;
    private final transient @NotNull IMirror<?> parent;
    private final transient @NotNull Object target;

    public MethodMirror(@NotNull Method method, @NotNull IMirror<?> parent, @NotNull Object target) {
        this.method = method;
        this.parent = parent;
        this.target = target;
        if (!method.isAccessible()) method.setAccessible(true);
        ReflectionCache.CACHE.cache(method, this);
    }

    public T invokeOn(Object target, Object... params) {
        try {
            return (T) method.invoke(target, params);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public T invoke(Object... params) {
        try {
            return (T) method.invoke(target, params);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public void invokeOn(Object target, @Nullable Consumer<T> success, @Nullable Consumer<Throwable> failure, Object... params) {
        try {
            if (success != null) success.accept(invokeOn(target, params));
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
        }
    }

    public void invoke(@Nullable Consumer<T> success, @Nullable Consumer<Throwable> failure, Object... params) {
        try {
            if (success != null) success.accept(invoke(params));
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
        }
    }

    public void invokeOn(Object target, Consumer<T> success, Object... params) {
        invokeOn(target, success, null, params);
    }

    public void invoke(Consumer<T> success, Object... params) {
        invoke(success, null, params);
    }

    public <Q extends Annotation> boolean hasAnnotation(Class<Q> annotation) {
        return method.getDeclaredAnnotation(annotation) != null;
    }

    public <Q extends Annotation> Q getAnnotation(Class<Q> annotation) {
        return method.getDeclaredAnnotation(annotation);
    }

    public int getParameterCount() {
        return method.getParameterCount();
    }

    public Class<?>[] getParameterTypes() {
        return method.getParameterTypes();
    }

    public Parameter[] getParameters() {
        return method.getParameters();
    }

    public boolean hasParameters() {
        return getParameterCount() == 0;
    }

    @Override
    public int getModifiers() {
        return method.getModifiers();
    }

    public Class<T> getReturnType() {
        return (Class<T>) method.getReturnType();
    }

    @Override
    public String getName() {
        return method.getName();
    }

    public boolean isStatic() {
        return Modifier.isStatic(method.getModifiers());
    }

    public boolean isFinal() {
        return Modifier.isFinal(method.getModifiers());
    }

    public boolean isPublic() {
        return Modifier.isPublic(method.getModifiers());
    }

    @Override
    public <Q> IMirror<Q> getParent() {
        return (IMirror<Q>) parent;
    }

    @Override
    public <Q> Q getTarget() {
        return (Q) target;
    }

    @Override
    public Method getLiteral() {
        return method;
    }
}
