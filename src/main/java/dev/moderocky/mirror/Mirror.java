package dev.moderocky.mirror;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Consumer;

@SuppressWarnings("unchecked")
public final class Mirror<T> implements Reflective, ClassWorker<T> {

    private final transient @NotNull T object;
    private final transient @NotNull Map<String, FieldMirror<?>> fields;

    public Mirror(@NotNull T object) {
        this.object = object;
        if (MirrorCache.CACHE.containsKey(object)) {
            this.fields = MirrorCache.CACHE.get(object).fields;
        } else {
            this.fields = new HashMap<>();
            MirrorCache.CACHE.put(object, this);
        }
//        this.fields = new HashMap<>();
    }

    public static boolean classExists(String classPath) {
        try {
            Class.forName(classPath);
            return true;
        } catch (Throwable throwable) {
            return false;
        }
    }

    public static Mirror<?> blank() {
        return new Mirror<>(Mirror.class);
    }

    public static <Q> Class<Q> getClass(String classPath) {
        try {
            return (Class<Q>) Class.forName(classPath);
        } catch (Throwable throwable) {
            return null;
        }
    }

    public static <Q> Mirror<Class<Q>> mirror(String classPath) {
        Class<Q> classy = getClass(classPath);
        if (classy == null) return null;
        return new Mirror<>(classy);
    }

    public <Q extends Annotation> boolean hasAnnotation(Class<Q> annotation) {
        return getAsClass().getDeclaredAnnotation(annotation) != null;
    }

    public <Q extends Annotation> Q getAnnotation(Class<Q> annotation) {
        return getAsClass().getDeclaredAnnotation(annotation);
    }

    @Override
    public int getModifiers() {
        return getAsClass().getModifiers();
    }

    public boolean isLocalClass() {
        return getAsClass().isLocalClass();
    }

    public boolean isMemberClass() {
        return getAsClass().isMemberClass();
    }

    public boolean isSynthetic() {
        return getAsClass().isSynthetic();
    }

    public boolean isAnonymous() {
        return getAsClass().isAnonymousClass();
    }

    public boolean isAnnotation() {
        return getAsClass().isAnnotation();
    }

    public boolean isInterface() {
        return getAsClass().isInterface();
    }

    @Override
    public boolean isStatic() {
        return Modifier.isStatic(getAsClass().getModifiers());
    }

    @Override
    public boolean isFinal() {
        return Modifier.isFinal(getAsClass().getModifiers());
    }

    @Override
    public boolean isPublic() {
        return Modifier.isPublic(getAsClass().getModifiers());
    }

    @Override
    public String getName() {
        return getAsClass().getName();
    }

    public <Q> Q newInstance() {
        for (ConstructorMirror<Object> mirror : getConstructorMirrors()) {
            if (mirror.hasParameters()) continue;
            return (Q) mirror.newInstance();
        }
        return null;
    }

    public <Q> Q instantiate(Object... params) {
        for (Constructor<?> constructor : getAsClass().getDeclaredConstructors()) {
            try {
                if (!constructor.isAccessible()) constructor.setAccessible(true);
                return (Q) constructor.newInstance(params);
            } catch (Throwable ignore) {
            }
        }
        return null;
    }

    public <Q> ConstructorMirror<Q> constructor(Constructor<Q> constructor) {
        if (ReflectionCache.CACHE.hasCache(constructor))
            return ReflectionCache.CACHE.getCache(constructor);
        return new ConstructorMirror<>(constructor, this, object);
    }

    public <Q> ConstructorMirror<Q> constructor(Class<?>... params) {
        try {
            Constructor<Q> constructor = (Constructor<Q>) getAsClass().getDeclaredConstructor(params);
            if (ReflectionCache.CACHE.hasCache(constructor))
                return ReflectionCache.CACHE.getCache(constructor);
            return new ConstructorMirror<>(constructor, this, object);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    @SuppressWarnings("all")
    public boolean hasConstructor(Class<?>... params) {
        try {
            return getAsClass().getDeclaredConstructor(params) != null;
        } catch (Throwable throwable) {
            return false;
        }
    }

    public <Q> void ifHasConstructor(Consumer<ConstructorMirror<Q>> consumer, Class<?>... params) {
        if (hasConstructor(params)) consumer.accept(constructor(params));
    }

    public <Q> FieldMirror<Q> field(String name) {
        if (fields.containsKey(name))
            return ((FieldMirror<Q>) fields.get(name));
        FieldMirror<Q> mirror = field(name, getAsClass());
        fields.put(name, mirror);
        return mirror;
    }

    public <Q> FieldMirror<Q> field(String name, Class<?> actor) {
        try {
            Field field = actor.getDeclaredField(name);
            return field(field);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public <Q> FieldMirror<Q> field(Field field) {
        if (ReflectionCache.CACHE.hasCache(field))
            return ReflectionCache.CACHE.getCache(field);
        return new FieldMirror<>(field, this, object);
    }

    public boolean hasField(String name) {
        return hasField(name, getAsClass());
    }

    @SuppressWarnings("all")
    public boolean hasField(String name, Class<?> actor) {
        try {
            return actor.getDeclaredField(name) != null;
        } catch (Throwable throwable) {
            return false;
        }
    }

    public <Q> void ifHasField(String name, Consumer<FieldMirror<Q>> consumer) {
        if (hasField(name)) consumer.accept(field(name));
    }

    public <Q> MethodMirror<Q> method(String name, Class<?> actor, Class<?>... params) {
        try {
            Method method = actor.getDeclaredMethod(name, params);
            return method(method);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public <Q> MethodMirror<Q> method(String name, Class<?>... params) {
        try {
            Method method = object instanceof Class ? ((Class<?>) object).getDeclaredMethod(name, params) : object.getClass().getDeclaredMethod(name, params);
            return method(method);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    @SuppressWarnings("all")
    public boolean hasMethod(String name, Class<?> actor, Class<?>... params) {
        try {
            return actor.getDeclaredMethod(name, params) != null;
        } catch (Throwable throwable) {
            return false;
        }
    }

    @SuppressWarnings("all")
    public boolean hasMethod(String name, Class<?>... params) {
        try {
            return object instanceof Class ? ((Class) object).getDeclaredMethod(name, params) != null : object.getClass().getDeclaredMethod(name, params) != null;
        } catch (Throwable throwable) {
            return false;
        }
    }

    public boolean hasMethod(String name, Object... params) {
        Set<Class<?>> classes = new HashSet<>();
        for (Object param : params) {
            if (param instanceof Class) classes.add((Class<?>) param);
            else classes.add(param.getClass());
        }
        return hasMethod(name, classes.toArray(new Class[0]));
    }

    public <Q> void ifHasMethod(String name, Class<?> actor, Consumer<MethodMirror<Q>> consumer, Class<?>... params) {
        if (hasMethod(name, actor, new Class[0])) consumer.accept(method(name, params));
    }

    public <Q> void ifHasMethod(String name, Consumer<MethodMirror<Q>> consumer, Class<?>... params) {
        if (hasMethod(name)) consumer.accept(method(name, params));
    }

    public <Q> MethodMirror<Q> method(String name, Object... params) {
        Set<Class<?>> classes = new HashSet<>();
        for (Object param : params) {
            if (param instanceof Class) classes.add((Class<?>) param);
            else classes.add(param.getClass());
        }
        return method(name, classes.toArray(new Class[0]));
    }

    public <Q> MethodMirror<Q> method(Method method) {
        if (ReflectionCache.CACHE.hasCache(method))
            return ReflectionCache.CACHE.getCache(method);
        return new MethodMirror<>(method, this, object);
    }

    public <Q extends Annotation> Method[] getMethods(Class<Q> annotation) {
        List<Method> methods = new ArrayList<>();
        for (Method method : getAsClass().getDeclaredMethods()) {
            if (method.getAnnotation(annotation) != null) methods.add(method);
        }
        return methods.toArray(new Method[0]);
    }

    public <Q extends Annotation> MethodMirror<?>[] getMethodMirrors(Class<Q> annotation) {
        Method[] methods = getMethods(annotation);
        MethodMirror<?>[] mirrors = new MethodMirror[methods.length];
        for (int i = 0; i < methods.length; i++) {
            mirrors[i] = method(methods[i]);
        }
        return mirrors;
    }

    public <Q extends Annotation> Field[] getFields(Class<Q> annotation) {
        List<Field> fields = new ArrayList<>();
        for (Field field : getAsClass().getDeclaredFields()) {
            if (field.getAnnotation(annotation) != null) fields.add(field);
        }
        return fields.toArray(new Field[0]);
    }

    public <Q extends Annotation> FieldMirror<?>[] getFieldMirrors(Class<Q> annotation) {
        Field[] fields = getFields(annotation);
        FieldMirror<?>[] mirrors = new FieldMirror[fields.length];
        for (int i = 0; i < fields.length; i++) {
            mirrors[i] = field(fields[i]);
        }
        return mirrors;
    }

    public <Q> ConstructorMirror<Q>[] getConstructorMirrors() {
        Constructor<Q>[] constructors = (Constructor<Q>[]) getAsClass().getDeclaredConstructors();
        ConstructorMirror<Q>[] mirrors = new ConstructorMirror[constructors.length];
        for (int i = 0; i < constructors.length; i++) {
            mirrors[i] = constructor(constructors[i]);
        }
        return mirrors;
    }

    public <Q> FieldMirror<Q>[] getFieldMirrors() {
        Field[] fields = getAsClass().getDeclaredFields();
        FieldMirror<Q>[] mirrors = new FieldMirror[fields.length];
        for (int i = 0; i < fields.length; i++) {
            mirrors[i] = field(fields[i]);
        }
        return mirrors;
    }

    public <Q> FieldMirror<Q>[] getFieldMirrorsFromActor(Class<?> actor) {
        Field[] fields = actor.getDeclaredFields();
        FieldMirror<Q>[] mirrors = new FieldMirror[fields.length];
        for (int i = 0; i < fields.length; i++) {
            mirrors[i] = field(fields[i]);
        }
        return mirrors;
    }

    public <Q> MethodMirror<Q>[] getMethodMirrors() {
        Method[] methods = getAsClass().getDeclaredMethods();
        MethodMirror<Q>[] mirrors = new MethodMirror[methods.length];
        for (int i = 0; i < methods.length; i++) {
            mirrors[i] = method(methods[i]);
        }
        return mirrors;
    }

    public <Q> MethodMirror<Q>[] getMethodMirrorsFromActor(Class<?> actor) {
        Method[] methods = actor.getDeclaredMethods();
        MethodMirror<Q>[] mirrors = new MethodMirror[methods.length];
        for (int i = 0; i < methods.length; i++) {
            mirrors[i] = method(methods[i]);
        }
        return mirrors;
    }

    public Method[] scanForMethods(Class<?> returnType, Boolean isStatic, Class<?>... params) {
        List<Method> methods = new ArrayList<>();
        for (Method method : getAsClass().getDeclaredMethods()) {
            boolean stat = Modifier.isStatic(method.getModifiers());
            if (isStatic != null) {
                if (stat ^ isStatic) continue;
            }
            Class<?> cls = method.getReturnType();
            if (method.getParameterCount() == params.length && (cls == returnType || returnType.isAssignableFrom(cls))) check: {
                int i = 0;
                for (Class<?> type : method.getParameterTypes()) {
                    if (type == params[i] || params[i].isAssignableFrom(type))
                        i++;
                    else break check;
                }
                methods.add(method);
            }
        }
        return methods.toArray(new Method[0]);
    }

    public MethodMirror<?>[] scanForMethodMirrors(Class<?> returnType, Class<?>... params) {
        List<MethodMirror<?>> methods = new ArrayList<>();
        for (Method method : scanForMethods(returnType, (object instanceof Class) ? true : null, params)) {
            boolean stat = Modifier.isStatic(method.getModifiers());
            methods.add(new MethodMirror<>(method, this, stat ? getAsClass() : object));
        }
        return methods.toArray(new MethodMirror[0]);
    }

    public Field[] scanForFields(Class<?> type, Boolean isStatic) {
        List<Field> fields = new ArrayList<>();
        for (Field field : getAsClass().getDeclaredFields()) {
            boolean stat = Modifier.isStatic(field.getModifiers());
            if (isStatic != null) {
                if (stat ^ isStatic) continue;
            }
            if (field.getType() == type || type.isAssignableFrom(field.getType()))
                fields.add(field);
        }
        return fields.toArray(new Field[0]);
    }

    public FieldMirror<?>[] scanForFieldMirrors(Class<?> type) {
        List<FieldMirror<?>> fields = new ArrayList<>();
        for (Field field : scanForFields(type, (object instanceof Class) ? true : null)) {
            boolean stat = Modifier.isStatic(field.getModifiers());
            fields.add(new FieldMirror<>(field, this, stat ? getAsClass() : object));
        }
        return fields.toArray(new FieldMirror[0]);
    }

    public <Q> Q invoke(String name, Object... params) {
        MethodMirror<Q> method = this.method(name, params);
        return method.invoke(params);
    }

    public <Q> Q invokeIf(String name, Object... params) {
        if (!hasMethod(name, params)) return null;
        return invoke(name, params);
    }

    public <Q> Mirror<Class<Q>> getInner(String name) {
        for (Class<?> innerClass : getInnerClasses()) {
            if (innerClass.getName().endsWith(name)) return new Mirror<>((Class<Q>) innerClass);
        }
        return null;
    }

    public Class<?>[] getInnerClasses() {
        return getAsClass().getDeclaredClasses();
    }

    public boolean hasInnerClasses() {
        return getAsClass().getDeclaredClasses().length > 0;
    }

    public boolean isEnum() {
        return getAsClass().isEnum();
    }

    public boolean isArray() {
        return getAsClass().isArray();
    }

    public T getEnum(String id) {
        Mirror<Class<?>> mirror = new Mirror<>(getAsClass());
        if (!mirror.isEnum()) return null;
        return mirror.invoke("valueOf", id);
    }

    public Package getPackage() {
        return new Package(getAsClass().getPackage().getName());
    }

    @Override
    public Mirror<T> getParent() {
        return this;
    }

    @Override
    public T getTarget() {
        return object;
    }

    @Override
    public Object getLiteral() {
        return object;
    }

    @Override
    public Object getNativeLiteral() {
        return getAsClass();
    }
}
