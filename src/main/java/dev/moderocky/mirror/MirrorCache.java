package dev.moderocky.mirror;

import java.lang.reflect.AccessibleObject;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.WeakHashMap;

class MirrorCache extends WeakHashMap<Object, Mirror<?>> {

    public static final MirrorCache CACHE = new MirrorCache();

    private MirrorCache() {

    }

}
