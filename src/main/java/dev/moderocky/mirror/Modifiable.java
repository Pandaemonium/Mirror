package dev.moderocky.mirror;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public interface Modifiable extends Reflective {

    static Modifiable check(Object object) {
        return new Modifiable() {
            @Override
            public int getModifiers() {
                return object instanceof Field ? ((Field) object).getModifiers() :
                        object instanceof Method ? ((Method) object).getModifiers() :
                                object instanceof Constructor ? ((Constructor<?>) object).getModifiers() :
                                        object.getClass().getModifiers();
            }

            @Override
            public Object getLiteral() {
                return object;
            }
        };
    }

    int getModifiers();

    default boolean isStatic() {
        return Modifier.isStatic(getModifiers());
    }

    default boolean isDynamic() {
        return !Modifier.isStatic(getModifiers());
    }

    default boolean isFinal() {
        return Modifier.isFinal(getModifiers());
    }

    default boolean isStrict() {
        return Modifier.isStrict(getModifiers());
    }

    default boolean isInterface() {
        return Modifier.isInterface(getModifiers());
    }

    default boolean isPublic() {
        return Modifier.isPublic(getModifiers());
    }

    default boolean isProtected() {
        return Modifier.isProtected(getModifiers());
    }

    default boolean isPackagePrivate() {
        return !isPublic() && !isPrivate() && !isProtected();
    }

    default boolean isPrivate() {
        return Modifier.isPrivate(getModifiers());
    }

    default boolean isAbstract() {
        return Modifier.isAbstract(getModifiers());
    }

    default boolean isTransient() {
        return Modifier.isTransient(getModifiers());
    }

    default boolean isNative() {
        return Modifier.isNative(getModifiers());
    }

    default boolean isSynchronized() {
        return Modifier.isSynchronized(getModifiers());
    }

    default boolean isVolatile() {
        return Modifier.isVolatile(getModifiers());
    }

    default boolean removeModifier(int modifier) {
        return modifyAccess(getModifiers() & ~modifier);
    }

    default boolean addModifier(int modifier) {
        return modifyAccess(getModifiers() & modifier);
    }

    default boolean modifyAccess(int modifiers) {
        return tryCatch(() -> new Mirror<>(getNativeLiteral()).field("modifiers").set(modifiers));
    }

    Object getLiteral();

    default Object getNativeLiteral() {
        return getLiteral();
    }

}
