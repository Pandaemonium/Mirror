package dev.moderocky.mirror;

public class Package {

    private final String path;

    public Package(String path) {
        this.path = path;
    }

    public <Q> Mirror<Class<Q>> getClass(String name) {
        return Mirror.mirror(path + "." + name);
    }

    public <Q> Class<Q> getClassRaw(String name) {
        return Mirror.getClass(path + "." + name);
    }

    public boolean hasClass(String name) {
        return Mirror.classExists(path + "." + name);
    }

    public java.lang.Package getJavaPackage() {
        return java.lang.Package.getPackage(path);
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return path;
    }

}
