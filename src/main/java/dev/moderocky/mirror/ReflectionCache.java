package dev.moderocky.mirror;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.WeakHashMap;

class ReflectionCache extends LinkedHashMap<byte[], IMirror<?>> {

    public static final ReflectionCache CACHE = new ReflectionCache();

    private ReflectionCache() {

    }

    boolean hasCache(AccessibleObject object) {
        return CACHE.containsKey(hash(object));
    }

    @SuppressWarnings("unchecked")
    <T extends IMirror<?>> T getCache(AccessibleObject object) {
        return (T) CACHE.get(hash(object));
    }

    <T extends IMirror<?>> void cache(AccessibleObject object, T mirror) {
        CACHE.put(hash(object), mirror);
    }

    byte[] hash(Object object) {
        return object.toString().getBytes(StandardCharsets.US_ASCII);
    }



}
