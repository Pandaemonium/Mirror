package dev.moderocky.mirror;

import dev.moderocky.mirror.logic.ReflectiveBiOperation;
import dev.moderocky.mirror.logic.ReflectiveOperation;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Function;

public interface Reflective {

    default boolean tryCatch(Runnable action) {
        return tryCatch(action, null);
    }

    default boolean tryCatch(Runnable action, @Nullable Consumer<Throwable> failure) {
        try {
            if (action != null) action.run();
            return true;
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
            return false;
        }
    }

    default boolean tryCatch(Consumer<Reflective> action, @Nullable Consumer<Throwable> failure) {
        try {
            if (action != null) action.accept(this);
            return true;
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
            return false;
        }
    }

    default <R> R tryCatchFunc(Function<Reflective, R> action, @Nullable Consumer<Throwable> failure) {
        try {
            return action.apply(this);
        } catch (Throwable throwable) {
            if (failure != null) failure.accept(throwable);
            return null;
        }
    }

    default <R> R tryFunc(Function<Reflective, R> action, R def) {
        try {
            return action.apply(this);
        } catch (Throwable throwable) {
            return def;
        }
    }

    default <Q> boolean attempt(ReflectiveOperation<Q> operation, Q obj) {
        try {
            operation.attempt(obj);
            return true;
        } catch (Throwable throwable) {
            return false;
        }
    }

    default <Q, R> R attempt(ReflectiveBiOperation<Q, R> operation, Q obj) {
        try {
            return operation.attempt(obj);
        } catch (Throwable throwable) {
            return operation.otherwise(obj, throwable);
        }
    }

}
