package dev.moderocky.mirror.functional;

import dev.moderocky.mirror.FieldMirror;
import dev.moderocky.mirror.Mirror;

import java.util.function.Consumer;

public abstract class ClassStateMirror<T> extends StateMirror<Class<T>> {

    private final Class<T> cls;
    private final Mirror<Class<T>> mirror;
    private final boolean error;

    public ClassStateMirror(Class<T> cls) {
        this.cls = cls;
        this.mirror = new Mirror<>(cls);
        this.error = false;
        readFields();
    }

    public ClassStateMirror(Class<T> cls, boolean throwErrors) {
        this.cls = cls;
        this.mirror = new Mirror<>(cls);
        this.error = throwErrors;
        readFields();
    }

    public final T construct(Object... params) {
        return mirror.tryCatchFunc(classIMirror -> mirror.instantiate(params), null);
    }

    @Override
    public final void readFields() {
        Mirror<ClassStateMirror<T>> thing = new Mirror<>(this);
        for (FieldMirror<Object> field : thing.getFieldMirrors()) {
            try {
                mirror.ifHasField(field.getName(), fieldMirror -> field.set(fieldMirror.get()));
            } catch (Throwable throwable) {
                if (error) throwable.printStackTrace();
            }
        }
    }

    @Override
    public final void writeFields() {
        Mirror<ClassStateMirror<T>> thing = new Mirror<>(this);
        for (FieldMirror<?> field : thing.getFieldMirrors()) {
            try {
                mirror.ifHasField(field.getName(), fieldMirror -> fieldMirror.set(field.get()));
            } catch (Throwable throwable) {
                if (error) throwable.printStackTrace();
            }
        }
    }

    @Override
    public final Class<T> getTarget() {
        return cls;
    }

    @Override
    public final Mirror<Class<T>> getMirror() {
        return mirror;
    }
}
