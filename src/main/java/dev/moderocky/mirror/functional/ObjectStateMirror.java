package dev.moderocky.mirror.functional;

import dev.moderocky.mirror.FieldMirror;
import dev.moderocky.mirror.Mirror;

public abstract class ObjectStateMirror<T> extends StateMirror<T> {

    private final T object;
    private final Mirror<T> mirror;
    private final boolean error;

    public ObjectStateMirror(T target) {
        this.object = target;
        this.mirror = new Mirror<>(target);
        this.error = false;
        readFields();
    }

    public ObjectStateMirror(T target, boolean throwErrors) {
        this.object = target;
        this.mirror = new Mirror<>(target);
        this.error = throwErrors;
        readFields();
    }

    @Override
    public final void readFields() {
        Mirror<ObjectStateMirror<T>> thing = new Mirror<>(this);
        for (FieldMirror<Object> field : thing.getFieldMirrors()) {
            try {
                mirror.ifHasField(field.getName(), fieldMirror -> field.set(fieldMirror.get()));
            } catch (Throwable throwable) {
                if (error) throwable.printStackTrace();
            }
        }
    }

    @Override
    public final void writeFields() {
        Mirror<ObjectStateMirror<T>> thing = new Mirror<>(this);
        for (FieldMirror<Object> field : thing.getFieldMirrors()) {
            field.ifHasMatching(mirror, fieldMirror -> fieldMirror.set(field.get()));
        }
    }

    @Override
    public final T getTarget() {
        return object;
    }

    @Override
    public final Mirror<T> getMirror() {
        return mirror;
    }
}
