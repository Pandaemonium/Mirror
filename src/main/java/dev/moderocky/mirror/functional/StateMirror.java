package dev.moderocky.mirror.functional;

import dev.moderocky.mirror.Mirror;

abstract class StateMirror<T> {

    public abstract Mirror<T> getMirror();

    public abstract T getTarget();

    public abstract void readFields();

    public abstract void writeFields();

    public final <Q> Q upstream(String methodName, Object... params) {
        return getMirror().invoke(methodName, params);
    }

}
