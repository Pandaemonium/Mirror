package dev.moderocky.mirror.logic;

import dev.moderocky.mirror.IMirror;
import dev.moderocky.mirror.Modifiable;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public enum Access {

    PUBLIC("public"),
    PROTECTED("protected"),
    PACKAGE_PRIVATE(""),
    PRIVATE("private");

    private final String keyword;
    Access(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public static Access of(Modifiable modifiable) {
        if (modifiable.isPublic()) return PUBLIC;
        if (modifiable.isPrivate()) return PRIVATE;
        if (Modifier.isProtected(modifiable.getModifiers())) return PROTECTED;
        return PACKAGE_PRIVATE;
    }

}
