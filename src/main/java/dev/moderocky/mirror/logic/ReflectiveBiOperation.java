package dev.moderocky.mirror.logic;

import org.jetbrains.annotations.Nullable;

@FunctionalInterface
public interface ReflectiveBiOperation<T, R> {

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    R attempt(T t) throws Throwable;

    default @Nullable R otherwise(T t, Throwable throwable) {
        return null;
    }

}
