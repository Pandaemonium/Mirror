package dev.moderocky.mirror.logic;

@FunctionalInterface
public interface ReflectiveOperation<T> {

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    void attempt(T t) throws Throwable;

}
